import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';
import '../utilities/styles/fd_colores.dart';
import 'home_page.dart';
import '../comandos/login_command.dart';
import 'package:art_sweetalert/art_sweetalert.dart';
class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool signin = true;
  bool _isLoading = false;
  late TextEditingController email, password;
  final LocalAuthentication auth = LocalAuthentication();

  void _handleLoginPressed({required String em, String, pw}) async {

    setState(() => _isLoading = true);
    bool success = await LoginCommand().run(em, pw);

    if(success){

      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) {
            return HomePage();
          }));
    }else{
      ArtSweetAlert.show(
          context: context,
          artDialogArgs: ArtDialogArgs(
              type: ArtSweetAlertType.danger,
              title: "ERROR DE USUARIO Y CONTRASEÑA",
              text: "NO SE HA PODIDO INICIAR SESIÓN"
          )
      );
      setState(() => _isLoading = false);
    }

  }

  @override
  void initState() {
    super.initState();
    authenticate();
    email = new TextEditingController();
    password = new TextEditingController();
  }

  authenticate() async {
    final canCheck = await auth.canCheckBiometrics;
    if (canCheck) {
      List<BiometricType> availableBiometrics =
          await auth.getAvailableBiometrics();
        if (availableBiometrics.contains(BiometricType.face)) {
          bool didAuthenticate =
              await auth.authenticate(localizedReason: 'Acceder por Face ID');
          if (didAuthenticate) {
            _handleLoginPressed(em: 'fondeadora', pw: 'fondeadora123');
          }
        } else if (availableBiometrics.contains(BiometricType.fingerprint)) {
          // Touch ID.
          bool didAuthenticate =
              await auth.authenticate(localizedReason: 'Acceder por Touch ID');
          if (didAuthenticate) {
            _handleLoginPressed(em: 'fondeadora', pw: 'fondeadora123');
          }
        }
    } else {
      print('Error');
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: _isLoading ? Container(
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.all(30.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(),
            ],
          ),
        ),
      ) : Container(
        child: Padding(
          padding: const EdgeInsets.all(30.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Iniciar Sesión',
                        style: TextStyle(
                            color: fd_negro,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Helvetica',
                            fontSize: 28),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(flex: 3, child:
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  TextField(
                    controller: email,
                    decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: fd_blanco,
                          ),
                        ),
                        hintText: 'Usuario',
                        hintStyle: TextStyle(
                          color: fd_coral.withOpacity(0.6),
                          fontFamily: 'Helvetica',
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color:  fd_negro,
                          ),
                        )),
                    style: TextStyle(
                      color: fd_negro,
                      fontFamily: 'Helvetica',
                      fontSize: 22.0,
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 100,
                  ),
                  TextField(
                    obscureText: true,
                    controller: password,
                    decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: fd_blanco,
                          ),
                        ),
                        hintText: 'Contraseña',
                        hintStyle: TextStyle(
                          color: fd_coral.withOpacity(0.6),
                          fontFamily: 'Helvetica',
                          fontSize: 22.0,
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                            color: fd_azul,
                          ),
                        )),
                    style: TextStyle(
                      color: fd_negro,
                      fontFamily: 'Helvetica',
                    ),
                  ),

                ],),

              ),
              Expanded(
                flex: 1,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          _handleLoginPressed(em: email.text, pw: password.text);
                        },
                        child: Container(
                          height: MediaQuery.of(context).size.height / 15,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                fd_coral,
                                fd_coral,
                              ],
                            ),
                            borderRadius: BorderRadius.circular(
                              30.0,
                            ),
                          ),
                          child: FittedBox(
                            fit: BoxFit.none,
                            child: Text(
                              'Acceder',
                              style: TextStyle(
                                color: fd_blanco,
                                fontSize: 20.0,
                                fontFamily: 'Helvetica',
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
