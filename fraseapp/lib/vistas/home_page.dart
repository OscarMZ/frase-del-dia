import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../comandos/refresh_posts_command.dart';
import '../main.dart';
import '../modelos/app_model.dart';
import '../modelos/user_model.dart';
import '../utilities/styles/fd_colores.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  void _handleRefreshPressed() async {
    //Ejecutar comando
    await RefreshPostsCommand().run(context.read<AppModel>().currentUser);
  }
  @override
  void initState() {
    super.initState();
    _handleRefreshPressed();
  }
  @override
  Widget build(BuildContext context) {
    // Vincula a UserModel.userPosts
    var frase = context.select<UserModel, String>((value) => value.userPosts);
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: fd_coral,
          ),
          onPressed: () => Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) {
                return MyApp();
              })),
        ),
        backgroundColor: Color.fromRGBO(255, 255, 255, 1.0),
        title: Text(
          "Cerrar Sesión",
          style: TextStyle(
            color: fd_negro,
            fontWeight: FontWeight.bold,
            fontFamily: 'Helvetica',
          ),
        ),
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(30.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        frase,
                        style: TextStyle(
                            color: fd_coral,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Helvetica',
                            fontSize: 28),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}