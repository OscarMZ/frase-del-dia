import 'dart:convert';
import 'package:http/http.dart' as http;

class UserService {
  Future<bool> login(String user, String pass) async {
    bool signin = false;

    if(user == 'fondeadora' && pass == 'fondeadora123'){
      signin = true;
    }
    return signin;
  }

  Future<String> getPosts(String user) async {
    return _ConsultarFrase();
  }

  Future<String> _ConsultarFrase() async {
    var url = "https://frasedeldia.azurewebsites.net/api/phrase";
    var client = http.Client();
    var res = await client.get(Uri.parse(url));
    var resp = jsonDecode(res.body);
    var frase = resp['phrase']+'\n'+'~'+resp['author'];
    return frase;
  }
}