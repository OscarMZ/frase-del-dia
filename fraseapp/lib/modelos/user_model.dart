import 'package:flutter/foundation.dart';

class UserModel extends ChangeNotifier {
  String? _userPosts ;
  String get userPosts => _userPosts??'';
  set userPosts(String userPosts) {
    _userPosts = userPosts;
    notifyListeners();
  }
}