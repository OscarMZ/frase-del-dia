import 'package:flutter/material.dart';
import 'package:fraseapp/utilities/styles/fd_colores.dart';
import 'package:provider/provider.dart';
import 'comandos/base_command.dart' as Commands;
import 'modelos/app_model.dart';
import 'modelos/user_model.dart';
import 'servicios/user_service.dart';
import 'vistas/login_page.dart';
import 'package:splash_screen_view/SplashScreenView.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext _) {

    Widget SplashFondeadora = SplashScreenView(
      navigateRoute: AppScaffold(),
      duration: 5000,
      imageSize: 130,
      imageSrc: "assets/logo_fondeadora_coral.png",
      text: "FRASE DEL DÍA",
      textType: TextType.ColorizeAnimationText,
      textStyle: TextStyle(
        fontSize: 40.0,
        fontFamily: 'Helvetica',
      ),
      colors: [
        fd_gris,
        fd_gris,
        fd_negro,
        fd_negro,
        fd_gris,
        fd_gris,
        fd_negro,
        fd_negro,
      ],
      backgroundColor: Colors.white,
    );
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (c) => AppModel()),
        ChangeNotifierProvider(create: (c) => UserModel()),
        Provider(create: (c) => UserService()),
      ],
      child: Builder(builder: (context) {
        // Guardamos un contexto que nuestros comandos puedan utilizar para acceder a los modelos y servicios proporcionados
        Commands.init(context);
        return MaterialApp(
            title: 'Fondeadora Frases',
            debugShowCheckedModeBanner: false,
            home: SplashFondeadora,
        );
      }),
    );
  }
}

class AppScaffold extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Vincula a AppModel.currentUser
    String currentUser = context.select<AppModel, String>((value) => value.currentUser);
    
    // Regresa la vista actual, basandose en el valor de currentUser:    
    return Scaffold(
      body: LoginPage(),
      //body: currentUser != null ? HomePage() : LoginPage(),
        //body: LoginPage(),
    );
  }
}